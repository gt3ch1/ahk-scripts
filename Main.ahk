﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#InstallKeybdHook
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%\Scripts\  ; Ensures a consistent starting directory.
Dir := "/Scripts/"
#include Scripts/Spotify.ahk
;#include Scripts/Windows&#.ahk
;#include Scripts/Suspend.ahk