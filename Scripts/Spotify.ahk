﻿SetTitleMatchMode,2 
DetectHiddenWindows, On

; Send a key to Spotify, or whatever is specified in the last parameter in ControlSend.

spotifyKey(key) {
	Send {~LCtrl down}
	ControlSend, ahk_exe, %key%, explorer.exe
	Send {~LCtrl up}
	Send {~LShift up}
	Return
}

^+Up::
{ 
	spotifyKey("{Up}")
	Return	
}



^+Down::
{
	spotifyKey("{Down}}")
	Return
}
^+M::
{
	spotifyKey("{F7}")
	Return
}
^+1::
{
	changeVolume("Google Play Music Desktop Player") 
	Return
}
^+sc039::Media_Play_Pause
Return
^+Left::Media_Prev
Return
^+Right::Media_Next
Return





changeVolume(program){

; Snatched from https://www.reddit.com/r/AutoHotkey/comments/65vpmb/adjust_volume_of_specific_programs_with_autohotkey/dgdo71a?utm_source=share&utm_medium=web2x
	run sndvol
	winwait Volume Mixer
	sleep 150

	winget, clist, ControlList, Volume Mixer
	pos := 1, m := ""
	while (pos := regexmatch(clist, "(Static\d{1,2})`nStatic\d{1,2}`n(\w+)", m, pos += strlen(m))) {
		controlgettext, text, % m1, Volume Mixer
		if (text = program) { ; your program name as it appears in the volume mixer
			sleep 30
			ControlFocus, % m2, Volume Mixer
			sleep 30
			Send {PgDn 5}
			sleep 10 
			Send {Up 8}
			break
		}
	}
	sleep 50
	winclose Volume Mixer
	Send {~LCtrl up}
	Send {~LShift up}
	return
}