﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
!+1::
Send {LWin down}
Send {1 down}
Send {LWin up}
Send {1 up}
Return

!+2::
Send {LWin down}
Send {2 down}
Send {LWin up}
Send {2 up}
Return

!+3::
Send {LWin down}
Send {3 down}
Send {LWin up}
Send {3 up}
Return

!+4::
Send {LWin down}
Send {4 down}
Send {LWin up}
Send {4 up}
Return

!+5::
Send {LWin down}
Send {5 down}
Send {LWin up}
Send {6 up}
Return

!+7::
Send {LWin down}
Send {7 down}
Send {LWin up}
Send {7 up}
Return

!+8::
Send {LWin down}
Send {8 down}
Send {LWin up}
Send {8 up}
Return

!+9::
Send {LWin down}
Send {9 down}
Send {LWin up}
Send {9 up}
Return
