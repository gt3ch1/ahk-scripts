﻿,#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#If GetKeyState("Scrolllock","T")REC 
Loop{
   if not GetKeyState("Scrolllock", "T")
	  Clipboard:=""
		
      	r::
			
			Clipboard:="REC "
			Send, {sc164}
			return
		f::
			Clipboard:="CUE "
			Send, %clipboard%
			return
		t::
			Clipboard:="@"
			Send, %clipboard%
			return
		d::
			Clipboard:="DELETE "
			Send, %clipboard%
			return
		e::
			Clipboard:="UPDATE "
			Send, %clipboard%
			return	
		y::
			Clipboard:="FULL "
			Send, %clipboard%
			Send, {Enter}
			return	
		h::
			Clipboard:="ON "
			Send, %clipboard%
			Send, {Enter}
			return
		n::
			Clipboard:="OUT "
			Send, %clipboard%
			Send, {Enter}
			return
		b::
			Clipboard:="BACK "
			Send, %clipboard%
			Send, {Enter}
			return
		a::
			Clipboard:="SC{+}"
			Send, %clipboard%
			Send, {Enter}
			return
		z::
			Clipboard:="SC -"
			Send, %clipboard%
			Send, {Enter}
			return
		v::
			Clipboard:="STOP"
			Send, %clipboard%
			Send, {Enter}
			return
		w::
			Clipboard:="GROUP "
			Send, %clipboard%
			return
		s::
			Clipboard:="SC "
			Send, %clipboard%
			return		
		q::
			Clipboard:="REL "
			Send, %clipboard%
			Send, {Enter}
			return
		g::
			Clipboard:="GO "
			Send, %clipboard%
			Send, {Enter}
			return
		1::
			Send, {F2}
			return
		2::
			Send, +{F2}
			return
		3::
			Send, {F3}
			return
		4::
			Send, {F4}
			return
		5::
			Send, {F5}
			return
		6::
			Send, {F6}
			return
		7::
			Send, {F7}
			return
		8::
			Send, {F8}
			return
		9::
			Send, {F10}
			return
		x::
			Send, {F12}
			return
		0::
			Send, +{F6}
			return
}
return