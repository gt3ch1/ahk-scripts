﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
SetTitleMatchMode, 2
WinWaitActive, Dolphin 5.0 | JIT64
	ControlSend, ahk_parent, ^{Down}, ahk_class GlovePIE
 	Send {shift Up}PIE{shift Down}
	